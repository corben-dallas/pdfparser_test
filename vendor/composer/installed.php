<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'jfcherng/php-color-output' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '2673074597eca9682d2fdfaee39a22418d4cc2f6',
    ),
    'jfcherng/php-diff' => 
    array (
      'pretty_version' => '6.9.0',
      'version' => '6.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'af34294eb1265b74863076ff1f07dabd83c24752',
    ),
    'jfcherng/php-mb-string' => 
    array (
      'pretty_version' => '1.4.3',
      'version' => '1.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ff8dacb993d83b5e8e2d8e325b5a015f3fb2da7d',
    ),
    'jfcherng/php-sequence-matcher' => 
    array (
      'pretty_version' => '3.2.6',
      'version' => '3.2.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '369933b7cfbc31979fd94bf6391451468f49c693',
    ),
    'smalot/pdfparser' => 
    array (
      'pretty_version' => 'v0.18.2',
      'version' => '0.18.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b6db6aa9f605e9a82a29f1325309929a1e0beac0',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5232de97ee3b75b0360528dae24e73db49566ab1',
    ),
  ),
);
