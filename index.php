<?php

include __DIR__ . '/vendor/autoload.php';

use Jfcherng\Diff\Differ;
use Jfcherng\Diff\Renderer\RendererConstant;
use Jfcherng\Diff\DiffHelper;
use Jfcherng\Utility\CliColor;

$parser = new Smalot\PdfParser\Parser();
$tarif_target = 346;
// the two sample files for comparison
$folder_4D = __DIR__ . '/4D/';
$folder_twig = __DIR__ . '/Twig/';

$simulation_folder_4D = array_values(array_diff(scandir($folder_4D), array('..', '.', '.DS_Store')));
$simulation_folder_twig = array_values(array_diff(scandir($folder_twig), array('..', '.', '.DS_Store')));

// options for Diff class
$diffOptions = [
    // show how many neighbor lines
    // Differ::CONTEXT_ALL can be used to show the whole file
    'context' => 1,
    // ignore case difference
    'ignoreCase' => false,
    // ignore whitespace difference
    'ignoreWhitespace' => false,
];

// options for renderer class
$rendererOptions = [
    // how detailed the rendered HTML is? (none, line, word, char)
    'detailLevel' => 'line',
    // renderer language: eng, cht, chs, jpn, ...
    // or an array which has the same keys with a language file
    'language' => 'eng',
    // show line numbers in HTML renderers
    'lineNumbers' => true,
    // show a separator between different diff hunks in HTML renderers
    'separateBlock' => true,
    // show the (table) header
    'showHeader' => true,
    // the frontend HTML could use CSS "white-space: pre;" to visualize consecutive whitespaces
    // but if you want to visualize them in the backend with "&nbsp;", you can set this to true
    'spacesToNbsp' => false,
    // HTML renderer tab width (negative = do not convert into spaces)
    'tabSize' => 4,
    // this option is currently only for the Combined renderer.
    // it determines whether a replace-type block should be merged or not
    // depending on the content changed ratio, which values between 0 and 1.
    'mergeThreshold' => 0.8,
    // this option is currently only for the Unified and the Context renderers.
    // RendererConstant::CLI_COLOR_AUTO = colorize the output if possible (default)
    // RendererConstant::CLI_COLOR_ENABLE = force to colorize the output
    // RendererConstant::CLI_COLOR_DISABLE = force not to colorize the output
    'cliColorization' => RendererConstant::CLI_COLOR_AUTO,
    // this option is currently only for the Json renderer.
    // internally, ops (tags) are all int type but this is not good for human reading.
    // set this to "true" to convert them into string form before outputting.
    'outputTagAsString' => false,
    // this option is currently only for the Json renderer.
    // it controls how the output JSON is formatted.
    // see available options on https://www.php.net/manual/en/function.json-encode.php
    'jsonEncodeFlags' => \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE,
    // this option is currently effective when the "detailLevel" is "word"
    // characters listed in this array can be used to make diff segments into a whole
    // for example, making "<del>good</del>-<del>looking</del>" into "<del>good-looking</del>"
    // this should bring better readability but set this to empty array if you do not want it
    'wordGlues' => [' ', '-'],
    // change this value to a string as the returned diff if the two input strings are identical
    'resultForIdenticals' => null,
    // extra HTML classes added to the DOM of the diff container
    'wrapperClasses' => ['diff-wrapper'],
];

$colorStyles = [
    'section' => ['f_black', 'b_cyan'],
    'message' => ['f_black', 'b_yellow']
];

$manyNewlines = "\n\n\n\n";

for ($i = 0; $i < count($simulation_folder_4D); $i++){
    var_dump($simulation_folder_4D);
    $simulation_pdf_4D = array_values(array_diff(scandir($folder_4D.$simulation_folder_4D[$i]), array('..', '.','.DS_Store')));
    $simulation_pdf_twig = array_values(array_diff(scandir($folder_twig.$simulation_folder_twig[$i]), array('..', '.', '.DS_Store')));
    if ((substr($simulation_pdf_twig[0],-3) === 'pdf') && (substr($simulation_pdf_4D[0],-3)  === 'pdf')){
        $oldString = $parser->parseFile($folder_4D.$simulation_folder_4D[$i].'/'.$simulation_pdf_4D[0])->getText();
        $newString = $parser->parseFile($folder_twig.$simulation_folder_twig[$i].'/'.$simulation_pdf_twig[0])->getText();
        // var_dump(str_replace('/', '' ,$oldString));
        if($oldString == $newString){
            echo CliColor::color("Les 2 fichiers sont identiques !", $colorStyles['message']) . "\n\n";
        } else {
            // generate a unified diff
            echo CliColor::color("Unified Diff\n============", $colorStyles['section']) . "\n\n";
            $unifiedResult = DiffHelper::calculate(
                $oldString,
                $newString,
                'Unified',
                $diffOptions,
                $rendererOptions
            );
            echo CliColor::color('Comparaison PDF de la liasse :  ' . $simulation_folder_4D[$i], $colorStyles['message']) . "\n\n";
            echo CliColor::color('simulation_directory_4D  --> '. $simulation_pdf_4D[0], $colorStyles['section']) . "\n";
            echo CliColor::color('simulation_directory_twig --> '. $simulation_pdf_twig[0], $colorStyles['section']) . "\n\n";
            echo $unifiedResult . $manyNewlines;
            // file_put_contents('diff_pdf'.$simulation_folder_twig[$i].'.diff', $unifiedResult);
        }
    }
    else {
        echo "Le fichier ".$i." n'est pas un PDF !";
    }
};


